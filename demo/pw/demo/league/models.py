from django.db import models

# Create your models here.


class League(models.Model):
	name = models.CharField(max_length = 24 )
	level = models.CharField(max_length = 24)
	number_pokemons = models.IntegerField()
	numer_medals = models.IntegerField()
	number_trainers = models.IntegerField()

	def __str__(self):
		return self.name
