from django.shortcuts import render

from django.views import generic
from .models import League 
from django.urls import reverse_lazy
# Create your views here.

# RETRIEVE

class List(generic.ListView):
	template_name = "league/list.html"
	model = League

class Detail(generic.DetailView):
	template_name = "league/detail.html"
	model = League

#Create 

class Create(generic.CreateView):
	template_name = "league/create.html"
	model = League
	fields = [
		"name",
		"level",
		"number_pokemons",
		"numer_medals",
		"number_trainers",
	]
	success_url = reverse_lazy("league_list")


# UPDATE

class Update(generic.UpdateView):
	template_name = "league/update.html"
	model = League
	fields = [
		"name",
		"level",
		"number_pokemons",
		"numer_medals",
		"number_trainers",
	]
	success_url = reverse_lazy("league_list")



# DELETE


class Delete(generic.DeleteView):
	template_name = "league/delete.html"
	model = League
	success_url = reverse_lazy("league_list")