from django.urls import path 

from league import views  

urlpatterns = [
	path('list/', views.List.as_view(), name="league_list"),
	path('detail/<int:pk>/', views.Detail.as_view(), name="league_detail"),
	path('create/', views.Create.as_view(), name="league_create"),
	path('update/<int:pk>/', views.Update.as_view(), name="league_update"),
	path('delete/<int:pk>/', views.Delete.as_view(), name="league_delete"),
]