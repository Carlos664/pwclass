from django.contrib import admin


from .models import Pokemon
# Register your models here.


#admin.site.register(Pokemon,AdminPokemon)

@admin.register(Pokemon)
class AdminPokemon(admin.ModelAdmin):
	list_display = [
		"pokemon_name",
		"pokemon_type",
		"pokemon_level",
		"image",
	]