from django import forms


from .models import Pokemon

class PokemonForm(forms.ModelForm):
	pokemon_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Pokemon Name", "class": "form-control"}))
	pokemon_type = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Pokemon Type", "class": "form-control"}))
	pokemon_level = forms.IntegerField(widget=forms.TextInput(attrs={"placeholder": "Pokemon Level", "class": "form-control"}))
	pokemon_exp = forms.IntegerField(widget=forms.TextInput(attrs={"placeholder": "Pokemon Exp", "class": "form-control"}))
	status = forms.BooleanField(widget=forms.CheckboxInput(attrs={"placeholder": "Pokemon Status", "class": "form-control"}))
	slug = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Slug", "class": "form-control"}))
	class Meta:
		model = Pokemon
		fields = [
			"pokemon_name",
			"pokemon_type",
			"pokemon_level",
			"pokemon_exp",
			"status",
			"slug",
		]