from django.db import models

# Create your models here.

from django.conf import settings

#user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)

class Pokemon(models.Model):
	trainer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	pokemon_name = models.CharField(max_length=24)
	pokemon_type = models.CharField(max_length=24)
	pokemon_level = models.IntegerField()
	pokemon_exp = models.IntegerField()
	status = models.BooleanField(default=True)
	timestamp = models.DateField(auto_now_add=True)
	image = models.ImageField(upload_to='img_pokemon', default='logoPython.png')
	slug = models.SlugField()

	def __str__(self):
		return self.pokemon_name



