from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

from django.http import JsonResponse, HttpResponse
from django.core import serializers

# Create your views here.

from .models import Pokemon
from .forms import PokemonForm

# CREATE

class Create(generic.CreateView):
	template_name = "pokedex/create2.html"
	model = Pokemon
	fields = [
		"pokemon_name",
		"pokemon_type",
		"pokemon_level",
		"pokemon_exp",
		"status",
		"slug",
	]
	success_url = reverse_lazy("list")

def create(request):
	form = PokemonForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "pokedex/create.html", context)





# RETRIEVE

class List(generic.ListView):
	template_name = "pokedex/list2.html"
	queryset = Pokemon.objects.filter(pokemon_type="Electric")


	def get_queryset(self, *args, **kwargs):
		qs = Pokemon.objects.all()
		print(self.request)
		query = self.request.GET.get("q", None)
		print(query)
		if query is not None:
			qs = qs.filter(Q(trainer__username__icontains=query) | Q(pokemon_name__icontains=query) | Q(pokemon_type__icontains=query))
		return qs


	def get_context_data(self, *args, **kwargs):
		context = super(List, self).get_context_data(*args, **kwargs)# context = get_context_data()
		context["message"] = "XD"
		#context["create_url"] = reverse_lazy("tweets:Create_Tweet_view")
		return context


def list(request):
	queryset = Pokemon.objects.all()
	context = {
		"pokemons": queryset
	}
	return render(request, "pokedex/list.html", context)



def wsList(request):
	data = serializers.serialize('json', Pokemon.objects.all())
	return HttpResponse(data, content_type="application/json")






class Detail(generic.DetailView):
	template_name = "pokedex/detail2.html"
	model = Pokemon




def detail(request, id):
 	queryset = Pokemon.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "pokedex/detail.html", context)


# UPDATE

class Update(generic.UpdateView):
	template_name = "pokedex/update2.html"
	model = Pokemon
	fields = [
		"pokemon_name",
		"pokemon_type",
		"pokemon_level",
		"pokemon_exp",
		"status",
		"slug",
	]
	success_url = reverse_lazy("list")



def update(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if  request.method == "GET":
		form = PokemonForm(instance=pokemon )
	else:
		form = PokemonForm(request.POST, instance=pokemon )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "pokedex/update.html", context)






# DELETE


class Delete(generic.DeleteView):
	template_name = "pokedex/delete2.html"
	model = Pokemon
	success_url = reverse_lazy("list")

def delete(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if request.method == "POST":
		pokemon.delete()
		return redirect("list")
	context = {
		"object": pokemon
	}
	return render(request, "pokedex/delete.html", context)










