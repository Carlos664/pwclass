from django.urls import path

from django.contrib.auth.views import logout_then_login

from home import views

urlpatterns = [

		path('', views.index, name="login"),
		path('about/', views.about, name="about"),
		path('lista/', views.lista, name="lista"),
		path('endpoint-users/', views.UserListAPIView.as_view(), name="UserSerializer"),
		path('endpoint-pokemon/', views.PokemonListAPIView.as_view(), name="PokemonSerializer"),
		path('cerrar/' , logout_then_login  , name = 'logout' ),
		path('signup/', views.Signup.as_view(), name="signup"),
]