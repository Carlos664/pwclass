from django.shortcuts import render
from django.contrib.auth.models import User

# rest_framework
from rest_framework import viewsets
from home.serializers import UserSerializer, PokemonSerializer
from rest_framework import generics

from django.views import generic

from pokedex.models import Pokemon

from .forms import Users_Form , LoginForm
from django.urls import reverse_lazy
from django.contrib.auth import authenticate , login

# Create your views here.


class UserListAPIView(generics.ListAPIView):
	serializer_class = UserSerializer
	queryset = User


	def get_queryset(self, *args, **kwargs):
		return User.objects.all()


class PokemonListAPIView(generics.ListAPIView):
	serializer_class = PokemonSerializer


	def get_queryset(self, *args, **kwargs):
		return Pokemon.objects.all()



def index(request):
	message = "Not Login"
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		form = LoginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request , user)
					message = "User Logged"
				else:
					message = "User is Not Active"
			else:
				message = "Username or Password is not Correct"
		context = {
			"name": "Ray Parra",
			"message": message,
			"result": 56 * 34,
			"form": form
		}
	context = {
		"form": form,
		"message": message
	}
	return render(request, "home/index.html", context)



def about(request):
	context = {
		"name": "Ray Parra",
		"message": "Ya se Rinden??",
		"result": 56 * 34,
	}
	return render(request, "home/about.html", context)


def lista(request):
	context = {
			"listaDatos": [1, 5, 8, 3, 9]
		}
	return render(request, "home/list.html", context)


class  Signup(generic.FormView):
	template_name = 'home/signup.html'
	form_class = Users_Form
	success_url = reverse_lazy('login')

	def form_valid(self , form):
		user = form.save()
		return super(Signup, self).form_valid(form) 
















