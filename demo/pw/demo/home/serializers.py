#serializers.py

from rest_framework import serializers
from django.contrib.auth.models import User

from pokedex.models import Pokemon

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			"id",
			"username",
			"last_name",
			"email",
		]


class PokemonSerializer(serializers.ModelSerializer):
	trainer = UserSerializer(read_only=True)
	class Meta:
		model = Pokemon
		fields = [
			"id",
			"pokemon_name",
			"pokemon_type",
			"trainer",
		]